﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace space_project
{
    public partial class Form1 : Form
    {
        Bridge childForm = new Bridge();
        richTextBox richbox = new richTextBox();

        public Form1()
        {
           InitializeComponent();
           WindowState = FormWindowState.Maximized;   
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rtb_Story.BackColor = Color.Black;
            rtb_Story.ForeColor = Color.LightBlue;
            rtb_Story.BorderStyle = BorderStyle.None;
           // rtb_Story.Dock = DockStyle.Top | DockStyle.Left;
            rtb_Story.ReadOnly = true;

            richbox.rtb_St = rtb_Story;
            richbox.aziz();

            keys.initializeKeys(10);
            Controls.Add(keys.gr_box_Keys);
        }

        int originalExStyle = -1;
        bool enableFormLevelDoubleBuffering = true;

        protected override CreateParams CreateParams
        {
            get
            {
                if (originalExStyle == -1)
                    originalExStyle = base.CreateParams.ExStyle;

                CreateParams cp = base.CreateParams;
                if (enableFormLevelDoubleBuffering)
                    cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                else
                    cp.ExStyle = originalExStyle;

                return cp;
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            RichTextBox richTextBox = (RichTextBox)sender;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button button = (Button)button1.Tag;

            if (childForm == null)
            {
                childForm = new Bridge(); 
                childForm.FormClosed += childForm_FormClosed;
            }

            childForm.Show(this);
            Hide();
        }

        void childForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            childForm = null;
            Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }
    }
}
