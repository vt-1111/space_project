﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace space_project
{
    internal class PanelGenerate : Form
    {  
        public Panel panel = new Panel();

        public void initializePanel(string pnlName)
        {
            this.panel.BackColor = System.Drawing.Color.Transparent;
            this.panel.BackgroundImage = global::space_project.Properties.Resources.Labor;
            this.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel.Location = new System.Drawing.Point(22, 22);
            this.panel.Name = pnlName;
            this.panel.Size = new System.Drawing.Size(1000, 700);
            this.panel.TabIndex = 10;
            this.panel.AutoSize = true; 
            this.panel.Dock = DockStyle.Fill;
        }

        
          

    }
}
