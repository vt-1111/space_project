﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace space_project
{
    internal class backButtonPanels : Button
    {
        public Button button = new Button();

            public void initializeButton()
        {
            this.button.BackColor = System.Drawing.Color.Transparent;
            this.button.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button.FlatAppearance.BorderSize = 0;
            this.button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.button.Location = new System.Drawing.Point(139, 50);
            this.button.Name = "back_button";
            this.button.Size = new System.Drawing.Size(181, 63);
            this.button.TabIndex = 9;
            this.button.TabStop = false;
            this.button.Text = "zurück";
            this.button.UseVisualStyleBackColor = false;
        }

    }
}
