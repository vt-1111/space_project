﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using space_project.Properties;
using System.Drawing;

namespace space_project
{

    public static class keys 
    {
        
        private static Panel gr_box_keys;
        private static PictureBox pb_keys;
        private static Label labelkey;
        public static int numOfkeys = 7;
        public static string stateText;

        public static Panel gr_box_Keys { get => gr_box_keys; set => gr_box_keys = value; }
        public static PictureBox Pb_keys { get => pb_keys; set => pb_keys = value; }
        public static Label labelKey { get => labelkey; set => labelkey = value; }

        public static void initializeKeys(int counter)
        {  
            Image image2 = global::space_project.Properties.Resources.BlueTextAreaTrans__1_;
            numOfkeys=counter;
            labelkey = new System.Windows.Forms.Label
            {
                AutoSize = true,
                // MinimumSize = new Size(50, 20),
                Location = new System.Drawing.Point(41, 54),
                Name = "lb_keys",
               // Size = new System.Drawing.Size(200, 200),
                TabIndex = 9,
                TabStop = false,
                Text = counter.ToString(),
                Font = new Font("Berlin Sans FB", 16),
                BackColor = System.Drawing.Color.Transparent,
                ForeColor = System.Drawing.Color.Black,
            };

            gr_box_keys = new Panel
            {
                AutoSize = false,
                BackColor = System.Drawing.Color.Transparent,
                Location = new System.Drawing.Point(1147, 30),
                Name = "",
                Size = new System.Drawing.Size(250, 149),
                TabIndex = 9,
                TabStop = false,
                BorderStyle = System.Windows.Forms.BorderStyle.None,
                BackgroundImage = image2,
                BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch,
                   
            };
            
            pb_keys = new System.Windows.Forms.PictureBox
            {
                BackColor = System.Drawing.Color.Transparent,
                BackgroundImage = global::space_project.Properties.Resources.Key__2_,
                BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch,
                Location = new System.Drawing.Point(132, 41),
                Name = "pb_key",
                Size = new System.Drawing.Size(93, 59),
                TabIndex = 9,
                TabStop = false
            };

            gr_box_keys.Controls.Add(pb_keys);
            gr_box_keys.Controls.Add(labelkey);

        }
    }
}
