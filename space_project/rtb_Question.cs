﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace space_project
{
    internal class rtb_Question
    {
        public RichTextBox rtbQuestion;

        public void initializeRtbQuestion()
        {
            this.rtbQuestion.BackColor = System.Drawing.Color.White;
            this.rtbQuestion.Location = new System.Drawing.Point(453, 215);
            this.rtbQuestion.Name = "rtbQuestion";
            this.rtbQuestion.Size = new System.Drawing.Size(648, 220);
            this.rtbQuestion.TabIndex = 10;
            this.rtbQuestion.Text = "";
        }
    }
}
