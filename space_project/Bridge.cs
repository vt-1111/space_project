﻿
using Microsoft.VisualBasic.FileIO;
using space_project.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace space_project
{
   

    internal class Bridge : Form
    {

        
        public static Bridge Instance;

        private void InitializeComponent()
        {
            this.schlaufraum = new System.Windows.Forms.Button();
            this.lb_floor = new System.Windows.Forms.Label();
            this.labor = new System.Windows.Forms.Button();
            this.kommandozentrale = new System.Windows.Forms.Button();
            this.kuche = new System.Windows.Forms.Button();
            this.lager = new System.Windows.Forms.Button();
            this.wohnbereich = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.pnl_schlafraum = new System.Windows.Forms.Panel();
            this.btn_sclafraum_back = new System.Windows.Forms.Button();
            this.pnl_kommandozentrale = new System.Windows.Forms.Panel();
            this.btn_kommandozentrale_back = new System.Windows.Forms.Button();
            this.pnl_kuche = new System.Windows.Forms.Panel();
            this.btn_kuche_back = new System.Windows.Forms.Button();
            this.pnl_nils_raum = new System.Windows.Forms.Panel();
            this.btn_question = new System.Windows.Forms.Button();
            this.rtb_question = new System.Windows.Forms.RichTextBox();
            this.btn_nils_raum_back = new System.Windows.Forms.Button();
            this.nils_raum = new System.Windows.Forms.Button();
            this.pnl_keys = new System.Windows.Forms.Panel();
            this.pb_keys = new System.Windows.Forms.PictureBox();
            this.lb_keys = new System.Windows.Forms.Label();
            this.btn_question_submit = new System.Windows.Forms.Button();
            this.lb_QA = new System.Windows.Forms.Label();
            this.pnl_Bonus = new System.Windows.Forms.Panel();
            this.btn_bonus3 = new System.Windows.Forms.Button();
            this.btn_bonus2 = new System.Windows.Forms.Button();
            this.btn_bonus1 = new System.Windows.Forms.Button();
            this.lb_bonus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnl_schlafraum.SuspendLayout();
            this.pnl_kommandozentrale.SuspendLayout();
            this.pnl_kuche.SuspendLayout();
            this.pnl_nils_raum.SuspendLayout();
            this.pnl_keys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_keys)).BeginInit();
            this.pnl_Bonus.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // schlaufraum
            // 
            this.schlaufraum.BackColor = System.Drawing.Color.Transparent;
            this.schlaufraum.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.schlaufraum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.schlaufraum.FlatAppearance.BorderSize = 0;
            this.schlaufraum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.schlaufraum.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.schlaufraum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.schlaufraum.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.schlaufraum.ForeColor = System.Drawing.Color.Black;
            this.schlaufraum.Location = new System.Drawing.Point(157, 81);
            this.schlaufraum.Name = "schlaufraum";
            this.schlaufraum.Size = new System.Drawing.Size(181, 63);
            this.schlaufraum.TabIndex = 0;
            this.schlaufraum.TabStop = false;
            this.schlaufraum.Text = "Schlaufraum";
            this.schlaufraum.UseVisualStyleBackColor = false;
            this.schlaufraum.Click += new System.EventHandler(this.schlaufraum_Click);
            this.schlaufraum.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.schlaufraum.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // lb_floor
            // 
            this.lb_floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb_floor.Location = new System.Drawing.Point(190, 130);
            this.lb_floor.Name = "lb_floor";
            this.lb_floor.Size = new System.Drawing.Size(784, 643);
            this.lb_floor.TabIndex = 1;
            // 
            // labor
            // 
            this.labor.BackColor = System.Drawing.Color.Transparent;
            this.labor.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.labor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.labor.FlatAppearance.BorderSize = 0;
            this.labor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.labor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.labor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labor.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labor.Location = new System.Drawing.Point(377, 177);
            this.labor.Name = "labor";
            this.labor.Size = new System.Drawing.Size(181, 63);
            this.labor.TabIndex = 2;
            this.labor.TabStop = false;
            this.labor.Text = "Labor";
            this.labor.UseVisualStyleBackColor = false;
            this.labor.Click += new System.EventHandler(this.labor_Click);
            this.labor.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.labor.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // kommandozentrale
            // 
            this.kommandozentrale.BackColor = System.Drawing.Color.Transparent;
            this.kommandozentrale.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.kommandozentrale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.kommandozentrale.FlatAppearance.BorderSize = 0;
            this.kommandozentrale.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.kommandozentrale.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.kommandozentrale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.kommandozentrale.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.kommandozentrale.Location = new System.Drawing.Point(377, 81);
            this.kommandozentrale.Name = "kommandozentrale";
            this.kommandozentrale.Size = new System.Drawing.Size(181, 63);
            this.kommandozentrale.TabIndex = 3;
            this.kommandozentrale.TabStop = false;
            this.kommandozentrale.Text = "Kommandozentrale";
            this.kommandozentrale.UseVisualStyleBackColor = false;
            this.kommandozentrale.Click += new System.EventHandler(this.kommandozentrale_Click);
            this.kommandozentrale.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.kommandozentrale.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // kuche
            // 
            this.kuche.BackColor = System.Drawing.Color.Transparent;
            this.kuche.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.kuche.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.kuche.FlatAppearance.BorderSize = 0;
            this.kuche.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.kuche.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.kuche.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.kuche.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.kuche.Location = new System.Drawing.Point(157, 177);
            this.kuche.Name = "kuche";
            this.kuche.Size = new System.Drawing.Size(181, 63);
            this.kuche.TabIndex = 4;
            this.kuche.TabStop = false;
            this.kuche.Text = "Küche";
            this.kuche.UseVisualStyleBackColor = false;
            this.kuche.Click += new System.EventHandler(this.kuche_Click);
            this.kuche.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.kuche.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // lager
            // 
            this.lager.BackColor = System.Drawing.Color.Transparent;
            this.lager.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.lager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lager.FlatAppearance.BorderSize = 0;
            this.lager.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.lager.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.lager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lager.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lager.Location = new System.Drawing.Point(377, 278);
            this.lager.Name = "lager";
            this.lager.Size = new System.Drawing.Size(181, 63);
            this.lager.TabIndex = 5;
            this.lager.TabStop = false;
            this.lager.Text = "Lager";
            this.lager.UseVisualStyleBackColor = false;
            this.lager.Click += new System.EventHandler(this.lager_Click);
            this.lager.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.lager.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // wohnbereich
            // 
            this.wohnbereich.BackColor = System.Drawing.Color.Transparent;
            this.wohnbereich.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.wohnbereich.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.wohnbereich.FlatAppearance.BorderSize = 0;
            this.wohnbereich.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.wohnbereich.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.wohnbereich.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.wohnbereich.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.wohnbereich.Location = new System.Drawing.Point(157, 278);
            this.wohnbereich.Name = "wohnbereich";
            this.wohnbereich.Size = new System.Drawing.Size(181, 63);
            this.wohnbereich.TabIndex = 6;
            this.wohnbereich.TabStop = false;
            this.wohnbereich.Text = "Wohnbereich";
            this.wohnbereich.UseVisualStyleBackColor = false;
            this.wohnbereich.Click += new System.EventHandler(this.wohnbereich_Click);
            this.wohnbereich.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.wohnbereich.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.Transparent;
            this.Exit.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Exit.Location = new System.Drawing.Point(377, 377);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(181, 63);
            this.Exit.TabIndex = 7;
            this.Exit.TabStop = false;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            this.Exit.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.Exit.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // pnl_schlafraum
            // 
            this.pnl_schlafraum.BackgroundImage = global::space_project.Properties.Resources.Blonde_Schlafraum;
            this.pnl_schlafraum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_schlafraum.Controls.Add(this.btn_sclafraum_back);
            this.pnl_schlafraum.Controls.Add(this.pnl_kommandozentrale);
            this.pnl_schlafraum.Location = new System.Drawing.Point(12, 398);
            this.pnl_schlafraum.Name = "pnl_schlafraum";
            this.pnl_schlafraum.Size = new System.Drawing.Size(950, 485);
            this.pnl_schlafraum.TabIndex = 8;
            // 
            // btn_sclafraum_back
            // 
            this.btn_sclafraum_back.BackColor = System.Drawing.Color.Transparent;
            this.btn_sclafraum_back.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.btn_sclafraum_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_sclafraum_back.FlatAppearance.BorderSize = 0;
            this.btn_sclafraum_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_sclafraum_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_sclafraum_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sclafraum_back.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_sclafraum_back.Location = new System.Drawing.Point(139, 50);
            this.btn_sclafraum_back.Name = "btn_sclafraum_back";
            this.btn_sclafraum_back.Size = new System.Drawing.Size(181, 63);
            this.btn_sclafraum_back.TabIndex = 0;
            this.btn_sclafraum_back.Text = "Zurück";
            this.btn_sclafraum_back.UseVisualStyleBackColor = false;
            this.btn_sclafraum_back.Click += new System.EventHandler(this.btn_sclafraum_back_Click);
            this.btn_sclafraum_back.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.btn_sclafraum_back.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // pnl_kommandozentrale
            // 
            this.pnl_kommandozentrale.BackColor = System.Drawing.Color.Black;
            this.pnl_kommandozentrale.BackgroundImage = global::space_project.Properties.Resources.Kommandozentrale;
            this.pnl_kommandozentrale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_kommandozentrale.Controls.Add(this.btn_kommandozentrale_back);
            this.pnl_kommandozentrale.Controls.Add(this.pnl_kuche);
            this.pnl_kommandozentrale.Location = new System.Drawing.Point(26, 50);
            this.pnl_kommandozentrale.Name = "pnl_kommandozentrale";
            this.pnl_kommandozentrale.Size = new System.Drawing.Size(950, 485);
            this.pnl_kommandozentrale.TabIndex = 10;
            // 
            // btn_kommandozentrale_back
            // 
            this.btn_kommandozentrale_back.BackColor = System.Drawing.Color.Transparent;
            this.btn_kommandozentrale_back.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.btn_kommandozentrale_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_kommandozentrale_back.FlatAppearance.BorderSize = 0;
            this.btn_kommandozentrale_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_kommandozentrale_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_kommandozentrale_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_kommandozentrale_back.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_kommandozentrale_back.Location = new System.Drawing.Point(139, 50);
            this.btn_kommandozentrale_back.Name = "btn_kommandozentrale_back";
            this.btn_kommandozentrale_back.Size = new System.Drawing.Size(181, 63);
            this.btn_kommandozentrale_back.TabIndex = 0;
            this.btn_kommandozentrale_back.Text = "Zurück";
            this.btn_kommandozentrale_back.UseVisualStyleBackColor = false;
            this.btn_kommandozentrale_back.Click += new System.EventHandler(this.btn_kommandozentrale_back_Click);
            this.btn_kommandozentrale_back.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.btn_kommandozentrale_back.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // pnl_kuche
            // 
            this.pnl_kuche.BackColor = System.Drawing.Color.Black;
            this.pnl_kuche.BackgroundImage = global::space_project.Properties.Resources.Küche;
            this.pnl_kuche.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_kuche.Controls.Add(this.btn_kuche_back);
            this.pnl_kuche.Location = new System.Drawing.Point(714, 109);
            this.pnl_kuche.Name = "pnl_kuche";
            this.pnl_kuche.Size = new System.Drawing.Size(499, 317);
            this.pnl_kuche.TabIndex = 8;
            // 
            // btn_kuche_back
            // 
            this.btn_kuche_back.BackColor = System.Drawing.Color.Transparent;
            this.btn_kuche_back.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.btn_kuche_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_kuche_back.FlatAppearance.BorderSize = 0;
            this.btn_kuche_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_kuche_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_kuche_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_kuche_back.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_kuche_back.Location = new System.Drawing.Point(139, 50);
            this.btn_kuche_back.Name = "btn_kuche_back";
            this.btn_kuche_back.Size = new System.Drawing.Size(181, 63);
            this.btn_kuche_back.TabIndex = 1;
            this.btn_kuche_back.Text = "Zurück";
            this.btn_kuche_back.UseVisualStyleBackColor = false;
            this.btn_kuche_back.Click += new System.EventHandler(this.btn_kuche_back_Click);
            this.btn_kuche_back.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.btn_kuche_back.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // pnl_nils_raum
            // 
            this.pnl_nils_raum.BackgroundImage = global::space_project.Properties.Resources.Niels_IT_Raum;
            this.pnl_nils_raum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_nils_raum.Controls.Add(this.btn_question);
            this.pnl_nils_raum.Controls.Add(this.rtb_question);
            this.pnl_nils_raum.Controls.Add(this.btn_nils_raum_back);
            this.pnl_nils_raum.Location = new System.Drawing.Point(24, 71);
            this.pnl_nils_raum.Name = "pnl_nils_raum";
            this.pnl_nils_raum.Size = new System.Drawing.Size(950, 485);
            this.pnl_nils_raum.TabIndex = 9;
            // 
            // btn_question
            // 
            this.btn_question.BackColor = System.Drawing.Color.Transparent;
            this.btn_question.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.btn_question.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_question.FlatAppearance.BorderSize = 0;
            this.btn_question.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_question.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_question.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_question.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_question.Location = new System.Drawing.Point(139, 136);
            this.btn_question.Name = "btn_question";
            this.btn_question.Size = new System.Drawing.Size(181, 63);
            this.btn_question.TabIndex = 11;
            this.btn_question.Text = "Fragen";
            this.btn_question.UseVisualStyleBackColor = false;
            this.btn_question.Click += new System.EventHandler(this.btn_question_Click);
            this.btn_question.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.btn_question.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // rtb_question
            // 
            this.rtb_question.BackColor = System.Drawing.Color.White;
            this.rtb_question.Location = new System.Drawing.Point(453, 215);
            this.rtb_question.Name = "rtb_question";
            this.rtb_question.Size = new System.Drawing.Size(648, 220);
            this.rtb_question.TabIndex = 10;
            this.rtb_question.Text = "";
            // 
            // btn_nils_raum_back
            // 
            this.btn_nils_raum_back.BackColor = System.Drawing.Color.Transparent;
            this.btn_nils_raum_back.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.btn_nils_raum_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_nils_raum_back.FlatAppearance.BorderSize = 0;
            this.btn_nils_raum_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_nils_raum_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_nils_raum_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_nils_raum_back.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_nils_raum_back.Location = new System.Drawing.Point(139, 50);
            this.btn_nils_raum_back.Name = "btn_nils_raum_back";
            this.btn_nils_raum_back.Size = new System.Drawing.Size(181, 63);
            this.btn_nils_raum_back.TabIndex = 0;
            this.btn_nils_raum_back.Text = "Zurück";
            this.btn_nils_raum_back.UseVisualStyleBackColor = false;
            this.btn_nils_raum_back.Click += new System.EventHandler(this.btn_nils_raum_back_Click);
            this.btn_nils_raum_back.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.btn_nils_raum_back.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // nils_raum
            // 
            this.nils_raum.BackColor = System.Drawing.Color.Transparent;
            this.nils_raum.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.nils_raum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.nils_raum.FlatAppearance.BorderSize = 0;
            this.nils_raum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.nils_raum.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.nils_raum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nils_raum.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.nils_raum.Location = new System.Drawing.Point(157, 377);
            this.nils_raum.Name = "nils_raum";
            this.nils_raum.Size = new System.Drawing.Size(181, 63);
            this.nils_raum.TabIndex = 9;
            this.nils_raum.TabStop = false;
            this.nils_raum.Text = "Nils Raum";
            this.nils_raum.UseVisualStyleBackColor = false;
            this.nils_raum.Click += new System.EventHandler(this.nils_raum_Click);
            this.nils_raum.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.nils_raum.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // pnl_keys
            // 
            this.pnl_keys.BackColor = System.Drawing.Color.Transparent;
            this.pnl_keys.BackgroundImage = global::space_project.Properties.Resources.BlueTextAreaTrans__1_;
            this.pnl_keys.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_keys.Controls.Add(this.pb_keys);
            this.pnl_keys.Controls.Add(this.lb_keys);
            this.pnl_keys.Location = new System.Drawing.Point(1446, 30);
            this.pnl_keys.Name = "pnl_keys";
            this.pnl_keys.Size = new System.Drawing.Size(250, 149);
            this.pnl_keys.TabIndex = 10;
            // 
            // pb_keys
            // 
            this.pb_keys.BackgroundImage = global::space_project.Properties.Resources.Key__2_;
            this.pb_keys.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_keys.Location = new System.Drawing.Point(132, 41);
            this.pb_keys.Name = "pb_keys";
            this.pb_keys.Size = new System.Drawing.Size(93, 59);
            this.pb_keys.TabIndex = 1;
            this.pb_keys.TabStop = false;
            // 
            // lb_keys
            // 
            this.lb_keys.AutoSize = true;
            this.lb_keys.Font = new System.Drawing.Font("Berlin Sans FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lb_keys.Location = new System.Drawing.Point(41, 54);
            this.lb_keys.Name = "lb_keys";
            this.lb_keys.Size = new System.Drawing.Size(74, 30);
            this.lb_keys.TabIndex = 0;
            this.lb_keys.Text = "label1";
            // 
            // btn_question_submit
            // 
            this.btn_question_submit.BackColor = System.Drawing.Color.Transparent;
            this.btn_question_submit.BackgroundImage = global::space_project.Properties.Resources.Blue_Button_Transparent;
            this.btn_question_submit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_question_submit.FlatAppearance.BorderSize = 0;
            this.btn_question_submit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_question_submit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_question_submit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_question_submit.Font = new System.Drawing.Font("Berlin Sans FB", 10.12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_question_submit.Location = new System.Drawing.Point(1275, 296);
            this.btn_question_submit.Name = "btn_question_submit";
            this.btn_question_submit.Size = new System.Drawing.Size(135, 38);
            this.btn_question_submit.TabIndex = 11;
            this.btn_question_submit.Text = "Antwort Zeigen";
            this.btn_question_submit.UseVisualStyleBackColor = false;
            this.btn_question_submit.Click += new System.EventHandler(this.btn_question_submit_Click);
            this.btn_question_submit.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            this.btn_question_submit.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            // 
            // lb_QA
            // 
            this.lb_QA.BackColor = System.Drawing.SystemColors.Control;
            this.lb_QA.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lb_QA.Location = new System.Drawing.Point(1279, 337);
            this.lb_QA.Name = "lb_QA";
            this.lb_QA.Size = new System.Drawing.Size(359, 333);
            this.lb_QA.TabIndex = 12;
            this.lb_QA.Text = "...........";
            this.lb_QA.Click += new System.EventHandler(this.lb_QA_Click);
            // 
            // pnl_Bonus
            // 
            this.pnl_Bonus.BackColor = System.Drawing.Color.Transparent;
            this.pnl_Bonus.BackgroundImage = global::space_project.Properties.Resources.txt_box;
            this.pnl_Bonus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_Bonus.Controls.Add(this.btn_bonus3);
            this.pnl_Bonus.Controls.Add(this.btn_bonus2);
            this.pnl_Bonus.Controls.Add(this.btn_bonus1);
            this.pnl_Bonus.Controls.Add(this.lb_bonus);
            this.pnl_Bonus.Font = new System.Drawing.Font("Berlin Sans FB", 16F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.pnl_Bonus.ForeColor = System.Drawing.Color.Lime;
            this.pnl_Bonus.Location = new System.Drawing.Point(985, 185);
            this.pnl_Bonus.Name = "pnl_Bonus";
            this.pnl_Bonus.Size = new System.Drawing.Size(308, 548);
            this.pnl_Bonus.TabIndex = 13;
            // 
            // btn_bonus3
            // 
            this.btn_bonus3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_bonus3.ForeColor = System.Drawing.Color.Black;
            this.btn_bonus3.Location = new System.Drawing.Point(39, 329);
            this.btn_bonus3.Name = "btn_bonus3";
            this.btn_bonus3.Size = new System.Drawing.Size(240, 60);
            this.btn_bonus3.TabIndex = 3;
            this.btn_bonus3.Text = "button3";
            this.btn_bonus3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_bonus3.UseVisualStyleBackColor = true;
            // 
            // btn_bonus2
            // 
            this.btn_bonus2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_bonus2.ForeColor = System.Drawing.Color.Black;
            this.btn_bonus2.Location = new System.Drawing.Point(39, 262);
            this.btn_bonus2.Name = "btn_bonus2";
            this.btn_bonus2.Size = new System.Drawing.Size(240, 60);
            this.btn_bonus2.TabIndex = 2;
            this.btn_bonus2.Text = "button2";
            this.btn_bonus2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_bonus2.UseVisualStyleBackColor = true;
            // 
            // btn_bonus1
            // 
            this.btn_bonus1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_bonus1.ForeColor = System.Drawing.Color.Black;
            this.btn_bonus1.Location = new System.Drawing.Point(39, 192);
            this.btn_bonus1.Name = "btn_bonus1";
            this.btn_bonus1.Size = new System.Drawing.Size(240, 60);
            this.btn_bonus1.TabIndex = 1;
            this.btn_bonus1.Text = "button1";
            this.btn_bonus1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_bonus1.UseVisualStyleBackColor = true;
            // 
            // lb_bonus
            // 
            this.lb_bonus.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lb_bonus.ForeColor = System.Drawing.Color.Red;
            this.lb_bonus.Location = new System.Drawing.Point(30, 39);
            this.lb_bonus.Name = "lb_bonus";
            this.lb_bonus.Size = new System.Drawing.Size(247, 148);
            this.lb_bonus.TabIndex = 0;
            this.lb_bonus.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::space_project.Properties.Resources.BlueTextAreaTrans__1_;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1174, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 149);
            this.panel1.TabIndex = 11;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::space_project.Properties.Resources.LebenMunze;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(154, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(55, 43);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(62, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "3";
            // 
            // Bridge
            // 
            this.BackgroundImage = global::space_project.Properties.Resources.Floor_Finish;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1461, 745);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl_Bonus);
            this.Controls.Add(this.pnl_nils_raum);
            this.Controls.Add(this.lb_QA);
            this.Controls.Add(this.btn_question_submit);
            this.Controls.Add(this.pnl_schlafraum);
            this.Controls.Add(this.pnl_keys);
            this.Controls.Add(this.nils_raum);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.wohnbereich);
            this.Controls.Add(this.lager);
            this.Controls.Add(this.kuche);
            this.Controls.Add(this.kommandozentrale);
            this.Controls.Add(this.labor);
            this.Controls.Add(this.schlaufraum);
            this.Controls.Add(this.lb_floor);
            this.Name = "Bridge";
            this.TransparencyKey = System.Drawing.Color.Black;
            this.Load += new System.EventHandler(this.Bridge_Load_1);
            this.pnl_schlafraum.ResumeLayout(false);
            this.pnl_kommandozentrale.ResumeLayout(false);
            this.pnl_kuche.ResumeLayout(false);
            this.pnl_nils_raum.ResumeLayout(false);
            this.pnl_keys.ResumeLayout(false);
            this.pnl_keys.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_keys)).EndInit();
            this.pnl_Bonus.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }
       
        public Button schlaufraum;
        public Label lb_floor;
        public Button labor;
        public Button kommandozentrale;
        public Button kuche;
        public Button lager;
        public Button wohnbereich;
        public Button Exit;
        private Panel pnl_schlafraum;
        private Button btn_sclafraum_back;
        public Button nils_raum;
        private Panel pnl_nils_raum;
        private Button btn_nils_raum_back;
        private Panel pnl_kommandozentrale;
        private Button btn_kommandozentrale_back;
        List<Panel> panelList = new List<Panel>();
        private Panel pnl_kuche;
        private Button btn_kuche_back;
        private Button btn_question;
        private RichTextBox rtb_question;

        PanelGenerate pnl_labor = new PanelGenerate();
        PanelGenerate pnl_wohnbereich = new PanelGenerate();
        PanelGenerate pnl_lager = new PanelGenerate();
        
        List<RadioButton> rb_list = new List<RadioButton>();
        List<string> questlist = new List<string>();
        List<string> posAnswerlist = new List<string>();
        List<string> neutAnswerlist = new List<string>();
        List<string> negAnswerlist = new List<string>();

        int[] indexAnswers = new int[3];

        radio_btn radiobutton = new radio_btn();

        public static int counter =7;

        private Panel pnl_keys;
        private Label lb_keys;
        private PictureBox pb_keys;
        private Button btn_question_submit;
        private Label lb_QA;
        public string csvSource = "";
        private Panel pnl_Bonus;
        private Label lb_bonus;
        private Button btn_bonus3;
        private Button btn_bonus2;
        private Button btn_bonus1;
        private Panel panel1;
        private PictureBox pictureBox1;
        private Label label1;
        bool say = true;
        bool questionAsked = false;
        bool bonusAsked = false;
        bool isInfected = false;
        bool isKilled = false;
        bool isWon = false;
        bool hasPanelNumber;
        int panelNumber = 1;
        int infectionNumber;

       
        

        public Bridge()
        {
            InitializeComponent();

            WindowState = FormWindowState.Maximized;
            //this.FormBorderStyle = FormBorderStyle.None;

            pnl_schlafraum.Visible = false;
            pnl_nils_raum.Visible = false;
            pnl_kommandozentrale.Visible = false;
            pnl_kuche.Visible = false;
            pnl_labor.Visible = false;
            pnl_wohnbereich.Visible = false;
            pnl_lager.Visible = false;
            


            lb_keys.Text = counter.ToString();
            pnl_keys.Visible = true;

            pnl_schlafraum.Dock = System.Windows.Forms.DockStyle.Fill;  
            pnl_nils_raum.Dock = System.Windows.Forms.DockStyle.Fill;
            pnl_kommandozentrale.Dock = System.Windows.Forms.DockStyle.Fill;
            pnl_kuche.Dock = System.Windows.Forms.DockStyle.Fill;

            
        }

        private void Bridge_Load_1(object sender, EventArgs e)
        {
            Random random = new Random();
            infectionNumber = 1;//random.Next(1, 8);

            keys.labelKey.Text = counter.ToString();
            keys.gr_box_Keys.BringToFront();

            this.Controls.Add(pnl_nils_raum);  
            this.Controls.Add(pnl_schlafraum);
            this.Controls.Add(pnl_kommandozentrale);
            this.Controls.Add(pnl_kuche);

            panel1.BringToFront();
            nils_raum.BringToFront();
            schlaufraum.BringToFront();
            Exit.BringToFront();
            labor.BringToFront();
            lager.BringToFront();
            wohnbereich.BringToFront();
            kommandozentrale.BringToFront();
            kuche.BringToFront();
            pnl_nils_raum.BringToFront();
            pnl_kommandozentrale.BringToFront();
            pnl_kuche.BringToFront();
            pnl_keys.BringToFront();
            pnl_Bonus.BringToFront();
            
            btn_sclafraum_back.BringToFront();
            btn_nils_raum_back.BringToFront();
            btn_kommandozentrale_back.BringToFront();
            btn_kuche_back.BringToFront();

            lb_floor.Controls.Add(nils_raum);
            lb_floor.Controls.Add(schlaufraum);
            lb_floor.Controls.Add(Exit);
            lb_floor.Controls.Add(labor);
            lb_floor.Controls.Add(lager);
            lb_floor.Controls.Add(wohnbereich);
            lb_floor.Controls.Add(kuche);
            lb_floor.Controls.Add(kommandozentrale);

            pnl_schlafraum.Controls.Add(btn_sclafraum_back);
            pnl_nils_raum.Controls.Add(btn_nils_raum_back);
            pnl_kommandozentrale.Controls.Add(btn_kommandozentrale_back);
            pnl_kuche.Controls.Add(btn_kuche_back);

            Image image = global::space_project.Properties.Resources.Raum_Menü_Background;

            lb_floor.Image = new Bitmap(image, lb_floor.Size);
            lb_floor.BackColor = System.Drawing.Color.Transparent;

            pnl_Bonus.Visible = false;
            rtb_question.Visible = false;
            rtb_question.BackColor = System.Drawing.Color.DarkBlue;
            rtb_question.ForeColor = Color.LightBlue;
            rtb_question.BorderStyle = BorderStyle.None;
            rtb_question.ReadOnly = true;

          
            lb_QA.Visible = false;
            btn_question_submit.Visible = false;
            panel1.Visible = true;

        }

        int originalExStyle = -1;
        bool enableFormLevelDoubleBuffering = true;

        protected override CreateParams CreateParams
        {
            get
            {
                if (originalExStyle == -1)
                    originalExStyle = base.CreateParams.ExStyle;

                CreateParams cp = base.CreateParams;
                if (enableFormLevelDoubleBuffering)
                    cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                else
                    cp.ExStyle = originalExStyle;

                return cp;
            }
        }

        
        public void Btn_MouseEnter(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            int larger = 10;
            int width = btn.Size.Width + larger;
            int height = btn.Size.Height + larger;

            if (say == true)
            {
                btn.Size = new System.Drawing.Size(width, height);
                say = false;
            }
        }

        public void Btn_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            int smaller = 10;
            int width = btn.Size.Width - smaller;
            int height = btn.Size.Height - smaller;

            if (say == false)
            {
                btn.Size = new System.Drawing.Size(width, height);
                say = true;
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }
   
        public void roomManager(string roomName)
        {
            if (roomName == "schlaufraum") pnl_schlafraum.Visible = true;
            if (roomName == "nils_raum") pnl_nils_raum.Visible = true;
            if (roomName == "kommandozentrale") pnl_kommandozentrale.Visible = true;
            if (roomName == "kuche") pnl_kuche.Visible = true;
            if (roomName == "labor") pnl_labor.panel.Visible = true;
            if (roomName == "wohnbereich") pnl_wohnbereich.panel.Visible = true;
            if (roomName == "lager") pnl_lager.panel.Visible = true;

            pnl_keys.Visible = false;

            Exit.Visible = false;
            wohnbereich.Visible = false;
            kuche.Visible = false;
            kommandozentrale.Visible = false;
            lager.Visible = false;
            labor.Visible = false;
            schlaufraum.Visible = false;
            lb_floor.Visible = false;
            panel1.Visible = false;

        }

        public void backClickHandler(string roomName)
        {
            if (roomName == "schlaufraum") pnl_schlafraum.Visible = false;
            if (roomName == "nils_raum") pnl_nils_raum.Visible = false;
            if (roomName == "kommandozentrale") pnl_kommandozentrale.Visible = false;
            if (roomName == "kuche") pnl_kuche.Visible = false;
            if (roomName == "labor") pnl_labor.panel.Visible = false;
            if (roomName == "wohnbereich") pnl_wohnbereich.panel.Visible = false;
            if (roomName == "lager") pnl_lager.panel.Visible = false;

            if (panelNumber > 7) panelNumber = 1;

            pnl_keys.Visible = true;

            Exit.Visible = true;
            wohnbereich.Visible = true;
            kuche.Visible = true;
            kommandozentrale.Visible = true;
            lager.Visible = true;
            labor.Visible = true;
            schlaufraum.Visible = true;
            lb_floor.Visible = true;
            panel1.Visible = true;

            rtb_question.Visible = false;

            lb_QA.Visible = false;
            btn_question_submit.Visible = false;
            lb_QA.Text = "";

            questionAsked = false;
            for (int i = 0; i < rb_list.Count; i++)
            {
                rb_list[i].Checked = false;
            }

            if (keys.numOfkeys == 0 & bonusAsked == true) Application.Exit();
            if (keys.numOfkeys == 0 & bonusAsked == false)
            {
                string message = "Möchten Sie Bonus Frage bekommen?";
                string caption = "Sie haben keine keys mehr";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                // Displays the MessageBox.
                result = MessageBox.Show(message, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes & bonusAsked == false)
                {
                    pnl_Bonus.Visible = true;
                    string datei = @"C:\Users\veli\Source\Repos\space_project\space_project\Resources\Fragen_Allgemein.csv";
                    string[] lines = System.IO.File.ReadAllLines(datei);

                    counter = 1;
                    keys.numOfkeys = counter;
                    keys.labelKey.Text = keys.numOfkeys.ToString();
                    lb_keys.Text = keys.labelKey.Text;
                    bonusAsked = true;
                } 
                if (result == System.Windows.Forms.DialogResult.No || keys.numOfkeys == 0 & bonusAsked == true)
                {
                    // Closes the parent form.
                    Application.Exit();
                }
            }

        }
        private void schlaufraum_Click(object sender, EventArgs e)
        {
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();
            lb_keys.Text = keys.labelKey.Text;
            pnl_schlafraum.Controls.Add(keys.gr_box_Keys);

            string roomname = "schlaufraum";
            roomManager(roomname);

            csvSource = "Fragen_Jana";
           
            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_schlafraum.Controls.Add(questBtn.button);
            pnl_schlafraum.Controls.Add(rtb_question);
            pnl_schlafraum.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_schlafraum.Controls.Add(lbl);
                lbl.Visible = false;
                panelNumber++;
            }
            panelList.Add(pnl_nils_raum);
        }

        private void btn_sclafraum_back_Click(object sender, EventArgs e)
        {
            string roomname = "schlaufraum";
            backClickHandler(roomname);
        }

        private void nils_raum_Click(object sender, EventArgs e)
        {
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();
            lb_keys.Text = keys.labelKey.Text;
            pnl_nils_raum.Controls.Add(keys.gr_box_Keys);

            string roomname = "nils_raum";
            roomManager(roomname);

            csvSource = "Fragen_Jana";

            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_nils_raum.Controls.Add(questBtn.button);
            pnl_nils_raum.Controls.Add(rtb_question);
            pnl_nils_raum.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);


            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_nils_raum.Controls.Add(lbl);
                lbl.Visible = false;
                panelNumber++;
            }
            panelList.Add(pnl_nils_raum);
        }

        private void btn_question_Click(object sender, EventArgs e)
        {
            rtb_question.Visible = true;
            rtb_question.BringToFront();
            rtb_question.ReadOnly = true;
            lb_QA.Visible = true;
            btn_question_submit.Visible = true;
            lb_QA.BringToFront();
            btn_question_submit.BringToFront();

            string datei = $@"C:\Users\veli\Source\Repos\space_project\space_project\Resources\{csvSource}.csv";
            string[] lines = System.IO.File.ReadAllLines(datei);

            for (int i = 0; i < 3; i++)
            {
                radiobutton.initializeRadioBtn();
                rb_list.Add(radiobutton.radioBtn);
            }

            for (int i = 0; i < 3; i++)
            {
                rtb_question.Controls.Add(rb_list[i]);
                rb_list[i].Location = new System.Drawing.Point(0, 0);
                rb_list[i].BringToFront();
            }

            Random random = new Random();
            string[] questionArray = new string[lines.Length];
            
            for (int j = 0; j < lines.Length; j++)
            {
                questionArray[j] = lines[j].Split(';')[0];
                questlist.Add(questionArray[j]);
                posAnswerlist.Add(lines[j].Split(';')[1]);
                neutAnswerlist.Add(lines[j].Split(';')[2]);
                negAnswerlist.Add(lines[j].Split(';')[3]);
            }
            rtb_question.Text = "";
            string[] choosenQuestionsArray = new string[3];
            rtb_question.SelectionFont = new Font("Berlin San FB", 12);
            rtb_question.Size = new Size(800, rtb_question.Size.Height);
            for (int j = 0; j < 3;)
            {
                int choosenQuestionsQuantity = random.Next(0, questionArray.Length);
                if (!choosenQuestionsArray.Contains(questionArray[choosenQuestionsQuantity]) && 
                    questionArray[choosenQuestionsQuantity]!="")
                {
                    indexAnswers[j] = choosenQuestionsQuantity;
                    choosenQuestionsArray[j] = questionArray[choosenQuestionsQuantity];
                    rb_list[j].Text = choosenQuestionsArray[j];
                    rb_list[j].Font = new Font("Berlin San FB", 12);
                    rb_list[j].Location = new System.Drawing.Point(rb_list[j].Location.X, rb_list[j].Location.Y + 50 * j);
                    j++;
                }
            }
        }

         private void btn_nils_raum_back_Click(object sender, EventArgs e)
        {
            string roomname = "nils_raum";
            backClickHandler(roomname);
        }
        
        private void kommandozentrale_Click(object sender, EventArgs e)
        {
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();

            lb_keys.Text = keys.labelKey.Text;

            pnl_kommandozentrale.Controls.Add(keys.gr_box_Keys);

            string roomname = "kommandozentrale";
            roomManager(roomname);

            csvSource = "Fragen_Elise";

            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_kommandozentrale.Controls.Add(questBtn.button);
            pnl_kommandozentrale.Controls.Add(rtb_question);
            pnl_kommandozentrale.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_kommandozentrale.Controls.Add(lbl);
                lbl.Visible = false;
                panelNumber++;
            }
            panelList.Add(pnl_kommandozentrale);
        }

        private void btn_kommandozentrale_back_Click(object sender, EventArgs e)
        {
            string roomname = "kommandozentrale";
            backClickHandler(roomname);
        }

        private void kuche_Click(object sender, EventArgs e)
        {
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();

            lb_keys.Text = keys.labelKey.Text;

            pnl_kuche.Controls.Add(keys.gr_box_Keys);

            string roomname = "kuche";
            roomManager(roomname);

            csvSource = "Fragen_Elise";

            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_kuche.Controls.Add(questBtn.button);
            pnl_kuche.Controls.Add(rtb_question);
            pnl_kuche.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_kuche.Controls.Add(lbl);
                lbl.Visible = false;
                panelNumber++;
            }
            panelList.Add(pnl_kuche);
        }


        private void btn_kuche_back_Click(object sender, EventArgs e)
        {
            string roomname = "kuche";
            backClickHandler(roomname);
        }

        private void labor_Click(object sender, EventArgs e)
        { 
            pnl_labor.initializePanel("pnl_labor");
            Controls.Add(pnl_labor.panel);

            backButtonPanels btn = new backButtonPanels();

            btn.initializeButton();
            pnl_labor.panel.Controls.Add(btn.button);
            btn.button.Click += new System.EventHandler(this.btn_labor_back_Click);
            btn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            btn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);
            
            
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();

            lb_keys.Text = keys.labelKey.Text;

            pnl_labor.panel.Controls.Add(keys.gr_box_Keys);

            string roomname = "labor";
            roomManager(roomname);

            csvSource = "Fragen_Elise";

            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_labor.panel.Controls.Add(questBtn.button);
            pnl_labor.panel.Controls.Add(rtb_question);
            pnl_labor.panel.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_labor.Controls.Add(lbl);
                lbl.Visible = false;
                panelNumber++;
            }
            panelList.Add(pnl_labor.panel);
        }

        private void btn_labor_back_Click(object sender, EventArgs e)
        {
            string roomname = "labor";
            backClickHandler(roomname);
        }

        private void wohnbereich_Click(object sender, EventArgs e)
        {
            pnl_wohnbereich.initializePanel("pnl_wohnbereich");
            pnl_wohnbereich.panel.BackgroundImage = global::space_project.Properties.Resources.Wohnbereich;
            Controls.Add(pnl_wohnbereich.panel);

            backButtonPanels btn = new backButtonPanels();

            btn.initializeButton();
            pnl_wohnbereich.panel.Controls.Add(btn.button);
            btn.button.Click += new System.EventHandler(this.btn_wohnbereich_back_Click);
            btn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            btn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();

            lb_keys.Text = keys.labelKey.Text;

            pnl_wohnbereich.panel.Controls.Add(keys.gr_box_Keys);

            string roomname = "wohnbereich";
            roomManager(roomname);

            csvSource = "Fragen_Elise";

            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_wohnbereich.panel.Controls.Add(questBtn.button);
            pnl_wohnbereich.panel.Controls.Add(rtb_question);
            pnl_wohnbereich.panel.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_wohnbereich.Controls.Add(lbl);
                lbl.Visible = false;
                panelNumber++;
            }

            panelList.Add(pnl_wohnbereich.panel);
        }

        private void btn_wohnbereich_back_Click(object sender, EventArgs e)
        {
            string roomname = "wohnbereich";
            backClickHandler(roomname);
        }

        private void lager_Click(object sender, EventArgs e)
        {
            pnl_lager.initializePanel("pnl_lager");
            pnl_lager.panel.BackgroundImage = global::space_project.Properties.Resources.Labor;
            Controls.Add(pnl_lager.panel);

            backButtonPanels btn = new backButtonPanels();

            btn.initializeButton();
            pnl_lager.panel.Controls.Add(btn.button);
            btn.button.Click += new System.EventHandler(this.btn_lager_back_Click);
            btn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            btn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

           
            counter--;
            keys.numOfkeys = counter;
            keys.labelKey.Text = keys.numOfkeys.ToString();

            lb_keys.Text = keys.labelKey.Text;

            pnl_lager.panel.Controls.Add(keys.gr_box_Keys);

            string roomname = "lager";
            roomManager(roomname);

            csvSource = "Fragen_Elise";

            questionHandling questBtn = new questionHandling();

            questBtn.initializeButton();
            pnl_lager.panel.Controls.Add(questBtn.button);
            pnl_lager.panel.Controls.Add(rtb_question);
            pnl_lager.panel.Controls.Add(btn_question_submit);
            questBtn.button.Click += new System.EventHandler(this.btn_question_Click);
            questBtn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            questBtn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            if(pnl_lager.panel.Controls["lbl"]!=null)
            {
                hasPanelNumber = true;
            }
            if (!hasPanelNumber)
            {
                Label lbl = new Label();
                lbl.Text = panelNumber.ToString();
                pnl_lager.Controls.Add(lbl);
                lbl.Visible = false;
                hasPanelNumber =false;
            }

            KillCharacter kill_btn = new KillCharacter();

            kill_btn.initializeButton();
            pnl_lager.panel.Controls.Add(kill_btn.button);
            kill_btn.button.Click += (sender, EventArgs) => { btn_kill_Click(sender, EventArgs, panelNumber.ToString()); };
            kill_btn.button.MouseEnter += new System.EventHandler(this.Btn_MouseEnter);
            kill_btn.button.MouseLeave += new System.EventHandler(this.Btn_MouseLeave);

            panelList.Add(pnl_lager.panel);
            panelNumber++;
        }

        private void btn_lager_back_Click(object sender, EventArgs e)
        {
            string roomname = "lager";
            backClickHandler(roomname);
        }

         void btn_kill_Click(object sender, EventArgs e, string str)
        {
            if (str == (infectionNumber + 1).ToString())
            {
                lager.Invoke((MethodInvoker)delegate { lager.Enabled = false; });
                btn_lager_back_Click(sender, e);

                string message = "Game Over!";
                string caption = "____Gewonnen!____";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                // Displays the MessageBox.
                result = MessageBox.Show(message, caption, buttons);
                if (result == DialogResult.OK) Application.Exit();
            } else
            {
                string message = "Sie haben falsche person getötet und ein Leben verloren!";
                string caption = "____Leider!____";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                // Displays the MessageBox.
                result = MessageBox.Show(message, caption, buttons);
                if(result == DialogResult.OK)
                {
                   int leben =  int.Parse(label1.Text);
                    leben -= 1;
                    label1.Text = leben.ToString();
                }

                lager.Invoke((MethodInvoker)delegate { lager.Enabled = false; });
                btn_lager_back_Click(sender, e);

                
            }
        }

    

        private void btn_question_submit_Click(object sender, EventArgs e)
        {
            if (rb_list[0].Checked == true & questionAsked == false)
            {
                lb_QA.Text = "\nFrage=>  " + rb_list[0].Text + "\n\nAntwort=> " + posAnswerlist[0];
                questionAsked = true;
                rb_list[0].Checked = false;
            }
            if(rb_list[1].Checked == true & questionAsked == false)
            {
                lb_QA.Text = "\n" + rb_list[1].Text + "\n\n" + posAnswerlist[1];
                questionAsked = true;
                rb_list[1].Checked = false;
            }
            if (rb_list[2].Checked == true & questionAsked == false)
            {
                lb_QA.Text = "\n" + rb_list[2].Text + "\n\n" + posAnswerlist[2];
                questionAsked = true;
                rb_list[2].Checked = false;
            }
        }

        private void lb_QA_Click(object sender, EventArgs e)
        {

        }
    }
}
